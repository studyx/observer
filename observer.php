<?php

/**
 * Interface Observer
 */
interface Observer{
    public function add(Company $subject);
    public function notify($price);
}

/**
 * Class stockSim
 */
class stockSim implements Observer {
    private $_companies;

    public function __construct()
    {
        $this->_companies = array();
    }

    /**
     * @param Company $subject
     */
    public function add(Company $subject)
    {
        array_push($this->_companies, $subject);
    }

    /**
     * update prices
     */
    public function updatePrices()
    {
        // calc new price
        $this->notify(rand(23.90, 88.39));
    }

    /**
     * @param $price
     */
    public function notify($price)
    {
        foreach ($this->_companies as $comp) {
            $comp->update($price + rand(23.90, 188.39));
        }
    }

}

/**
 * Interface Company
 */
interface Company{
    public function update($price);
}

/**
 * Class companyFactory Factory pattern
 */
class companyFactory
{
    public static function create($name, $price) {
    $class = ucfirst($name);
    return new $name($price);
    }
}


/**
 * Class Google
 */
class Google implements Company
{
    private $_price;

    /**
     * @param $price
     */
    public function __construct($price)
    {
        $this->_price = $price;
        echo "<p>Creating Google at $ ".$price ."</p>";
    }

    /**
     * @param $price
     */
    public function update($price)
    {
        $this->_price = $price;
        echo "<p>Google setting for $ {$price}</p>";
    }

}

/**
 * Class Yahoo
 */
class Yahoo implements Company
{
    private $_price;

    /**
     * @param $price
     */
    public function __construct($price)
    {
        $this->_price = $price;
        echo "<p>Creating Yahoo at $ ".$price ."</p>";
    }

    /**
     * @param $price
     */
    public function update($price)
    {
        $this->_price = $price;
        echo "<p>Yahoo setting for $ {$price}</p>";
    }

}

$stocksim = new StockSim();

/*$comp1 = new Google('19.99');
$comp2 = new Yahoo('39.99');*/

$comp1 = companyFactory::create('Google', 19.99);
$comp2 = companyFactory::create('Yahoo', 39.99);

$stocksim->add($comp1);
$stocksim->add($comp2);

$stocksim->updatePrices();